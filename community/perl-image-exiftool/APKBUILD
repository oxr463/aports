# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=perl-image-exiftool
# Bump only to versions that are marked as "production release" on
# https://exiftool.org/history.html.
pkgver=12.58
pkgrel=0
pkgdesc="Perl module for editing exif metadata in files"
url="https://exiftool.org/"
arch="noarch"
license="Artistic-1.0-Perl GPL-1.0-or-later"
depends="perl"
subpackages="$pkgname-doc exiftool"
source="https://exiftool.org/Image-ExifTool-$pkgver.tar.gz"
builddir="$srcdir/Image-ExifTool-$pkgver"

# secfixes:
#   12.40-r0:
#     - CVE-2022-23935
#   12.24-r0:
#     - CVE-2021-22204

build() {
	PERL_MM_USE_DEFAULT=1 perl Makefile.PL INSTALLDIRS=vendor
	make
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install

	# creates file collision among perl modules
	find "$pkgdir" -name perllocal.pod -delete
	find "$pkgdir" -name .packlist -delete
}

exiftool() {
	pkgdesc="Tool for editing exif metadata in files"
	depends="perl-image-exiftool=$pkgver-r$pkgrel"

	amove usr/bin
}

sha512sums="
b3eccbff2502c30528793b31683153c8100d02de458bfefbf2ffc152d909d52b7bf72839f5ef540ae87161c83cb79f2e0ac448b7b8357a03b6f9eb661a3a97f8  Image-ExifTool-12.58.tar.gz
"
