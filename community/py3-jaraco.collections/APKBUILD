# Contributor: Jiri Kastner <cz172638@gmail.com>
# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-jaraco.collections
pkgver=4.0.0
pkgrel=0
pkgdesc="jaraco - Module for text manipulation"
url="https://github.com/jaraco/jaraco.collections"
arch="noarch"
license="MIT"
depends="py3-jaraco.classes py3-jaraco.text"
# py3-setuptools_scm is needed to set python module version
makedepends="py3-gpep517 py3-setuptools py3-setuptools_scm py3-wheel"
checkdepends="py3-pytest"
source="https://files.pythonhosted.org/packages/source/j/jaraco.collections/jaraco.collections-$pkgver.tar.gz"
builddir="$srcdir/jaraco.collections-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" dist/jaraco.collections-$pkgver-py3-none-any.whl
}

sha512sums="
0f30714d87ef85705dc3330aa1d4912e73f55718257d1c0f0ad0569b840d420d0838111eae50400df50ae2ce11c0b903943951a7cd1391acdb331afb5f76198e  jaraco.collections-4.0.0.tar.gz
"
