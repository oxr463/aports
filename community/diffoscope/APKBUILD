# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=diffoscope
pkgver=239
pkgrel=0
pkgdesc="In-depth comparison of files, archives, and directories"
url="https://diffoscope.org/"
arch="noarch"
license="GPL-3.0-or-later"
depends="
	py3-libarchive-c
	py3-magic
	python3
	"
makedepends="
	py3-docutils
	py3-gpep517
	py3-setuptools
	py3-wheel
	python3-dev
	"
checkdepends="
	bzip2
	cdrkit
	gzip
	libarchive-tools
	openssh-client-default
	py3-html2text
	py3-pytest
	py3-pytest-xdist
	unzip
	"
source="https://salsa.debian.org/reproducible-builds/diffoscope/-/archive/$pkgver/diffoscope-$pkgver.tar.gz"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	# html test fails
	PYTHONDONTWRITEBYTECODE=1 \
	testenv/bin/python3 -m pytest -n auto -k 'not test_diff'
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
84160e6f42ed62b800939ef7ad11cf455deca80154579b944dd7f9b12f7609860e1d7f6d10fc880e1ad1ad24484b61b7074e93396f1ed26bff2a49e99f27e259  diffoscope-239.tar.gz
"
