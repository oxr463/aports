# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qt6-qttools
pkgver=6.4.3
pkgrel=3
_llvmver=16
pkgdesc="A cross-platform application and UI framework (Development Tools, QtHelp)"
url="https://qt.io/"
arch="all"
license="LGPL-2.1-only AND LGPL-3.0-only AND GPL-3.0-only AND Qt-GPL-exception-1.0"
depends_dev="
	qt6-qtdeclarative-dev
	vulkan-headers
	"
# -static not used but needed for cmake
makedepends="$depends_dev
	clang$_llvmver-dev
	clang$_llvmver-static
	cmake
	llvm$_llvmver-dev
	llvm$_llvmver-static
	perl
	samurai
	"
subpackages="$pkgname-dev"
options="!check" # No tests
builddir="$srcdir/qttools-everywhere-src-${pkgver/_/-}"

case $pkgver in
	*_alpha*|*_beta*|*_rc*) _rel=development_releases;;
	*) _rel=official_releases;;
esac

source="https://download.qt.io/$_rel/qt/${pkgver%.*}/${pkgver/_/-}/submodules/qttools-everywhere-src-${pkgver/_/-}.tar.xz"

build() {
	# their cmake uses the clang_include_dirs only, but our llvm include dirs
	# are in another castle
	export CXXFLAGS="$CXXFLAGS -I/usr/include/llvm$_llvmver"
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DINSTALL_PUBLICBINDIR=usr/bin
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	cd "$pkgdir"
	mkdir -p usr/bin

	while read -r line; do
		ln -s $line
	done < "$builddir"/build/user_facing_tool_links.txt
}

sha512sums="
24dc02b760d4b1640eac539c9d0dfff9fe516332e5932f43456140daa3044c1e748731a883cc4f80e94626602241d040341e2af27efd470e7d6f50a908660382  qttools-everywhere-src-6.4.3.tar.xz
"
