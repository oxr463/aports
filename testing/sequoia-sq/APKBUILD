# Maintainer: psykose <alice@ayaya.dev>
pkgname=sequoia-sq
pkgver=0.28.0
pkgrel=0
pkgdesc="Command-line frontends for Sequoia"
url="https://gitlab.com/sequoia-pgp/sequoia-sq"
# a completely unfixable time_t deprecation warning is an error on the builders?
#arch="all"
license="GPL-2.0-or-later"
makedepends="
	bzip2-dev
	cargo
	clang15-libclang
	nettle-dev
	openssl-dev
	"
checkdepends="bash"
subpackages="$pkgname-doc"
source="https://gitlab.com/sequoia-pgp/sequoia/-/archive/sq/v$pkgver/sequoia-sq-v$pkgver.tar.gz"
builddir="$srcdir/sequoia-sq-v$pkgver"

export CARGO_REGISTRIES_CRATES_IO_PROTOCOL="sparse"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
	cd sq
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cd sq
	cargo build --frozen --release
}

check() {
	cd sq
	cargo test --frozen
}

package() {
	install -Dm755 target/release/sq \
		-t "$pkgdir"/usr/bin/
	cd sq
	install -Dm644 man-sq/*.1 \
		-t "$pkgdir"/usr/share/man/man1/
}

sha512sums="
03f9dd6d345e21fd7f3362ad670266e4ec557ad97054c56c4779cc9316eb67ebdf19d6584e65c14a9b411e39938edb2d706fd4ef8ed38e94c8a1a069f87745dd  sequoia-sq-v0.28.0.tar.gz
"
