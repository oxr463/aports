# Contributor: Thomas Boerger <thomas@webhippie.de>
# Contributor: Gennady Feldman <gena01@gmail.com>
# Contributor: Sergii Sadovyi <serg.sadovoi@gmail.com>
# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Thomas Boerger <thomas@webhippie.de>
pkgname=terraform
pkgver=1.4.2
pkgrel=0
pkgdesc="Building, changing and combining infrastructure safely and efficiently"
url="https://www.terraform.io/"
arch="all"
license="MPL-2.0"
makedepends="go"
checkdepends="openssh-client"
source="$pkgname-$pkgver.tar.gz::https://github.com/hashicorp/terraform/archive/refs/tags/v$pkgver.tar.gz"
options="net"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v -o bin/$pkgname \
		-ldflags "-X main.GitCommit=v$pkgver -X github.com/hashicorp/terraform/version.Prerelease="
}

check() {
	case "$CARCH" in
	arm*|x86)
		go list . | xargs -t -n4 \
			go test -timeout=2m -parallel=4
		;;
	*)
		go test ./...
		;;
	esac
	bin/$pkgname -v
}

package() {
	install -Dm755 "$builddir"/bin/$pkgname -t "$pkgdir"/usr/bin
}

sha512sums="
66b937dae65c12eddfca9a4b95294fe5fff2ccb79f2afcd8ec47d227e0a36f8edba4190939d59c80a78a0752c42e43085e30f09ea66fd06531dc9cfe30fd78f2  terraform-1.4.2.tar.gz
"
