# Contributor: Lauren N. Liberda <lauren@selfisekai.rocks>
# Maintainer: Lauren N. Liberda <lauren@selfisekai.rocks>
pkgname=rocketchat-desktop
pkgver=3.9.0
pkgrel=0
pkgdesc="Official Desktop Client for Rocket.Chat"
url="https://github.com/RocketChat/Rocket.Chat.Electron"
arch="aarch64 x86_64"	# electron
license="MIT"
depends="electron"
makedepends="
	electron-dev
	electron-tasje
	nodejs
	npm
	vips-dev
	yarn
"
options="net !check"	# dev dependencies purged before check
source="
	https://github.com/RocketChat/Rocket.Chat.Electron/archive/refs/tags/$pkgver/rocketchat-desktop-$pkgver.tar.gz
	yarn-lockfile-fix.patch

	rocketchat-desktop
"
builddir="$srcdir/Rocket.Chat.Electron-$pkgver"

export ELECTRON_OVERRIDE_DIST_PATH=/usr/bin

prepare() {
	default_prepare

	yarn install --ignore-scripts --frozen-lockfile
}

build() {
	NODE_ENV=production \
	NODE_OPTIONS=--openssl-legacy-provider \
	yarn build

	yarn install --ignore-scripts --frozen-lockfile --production

	npm rebuild sharp --nodedir=/usr/include/electron/node_headers --build-from-source

	tasje pack --config electron-builder.json
}

package() {
	install -Dm644 tasje_out/resources/app.asar "$pkgdir"/usr/lib/$pkgname/app.asar

	install -Dm755 "$srcdir"/$pkgname "$pkgdir"/usr/bin/$pkgname

	install -Dm644 tasje_out/rocketchat.desktop "$pkgdir"/usr/share/applications/rocketchat.desktop
	while read -r size; do
		install -Dm644 tasje_out/icons/$size.png "$pkgdir"/usr/share/icons/hicolor/$size/apps/$pkgname.png
	done < tasje_out/icons/size-list
}

sha512sums="
f61d9e1bd66762088b8c4ef74d904f5418c496441790df5a0088300304cd9865c642a528cf22afbf194f690012d9f5a1feaabbe2f706cb61556bf9e44afec357  rocketchat-desktop-3.9.0.tar.gz
4a4c816a57560fcd0f5c59a8e9a04a09e9bf79a0973b7c8fbefae28b8e4dffd4ae4cfa5b5a59d24558e7456b6b2852b8f35f5a3aacde1f37447bccc5c288c329  yarn-lockfile-fix.patch
268648ff0cf707ab67d5ad2e8291308f6815ff185e84f85455eebfda7ede228333e405705ecb743fe4a0b5e88f73aab46866aba5c7316f319161199427199695  rocketchat-desktop
"
