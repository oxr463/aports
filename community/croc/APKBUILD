# Contributor: André Klitzing <aklitzing@@gmail.com>
# Maintainer: André Klitzing <aklitzing@gmail.com>
pkgname=croc
pkgver=9.6.4
pkgrel=0
pkgdesc="Easily and securely send things from one computer to another"
url="https://github.com/schollz/croc"
license="MIT"
arch="all"
makedepends="go"
source="https://github.com/schollz/croc/archive/refs/tags/v$pkgver/croc-$pkgver.tar.gz"
options="net" # fetch dependencies

# secfixes:
#   9.1.0-r0:
#     - CVE-2021-31603

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	export CGO_LDFLAGS="$LDFLAGS"
	export GOFLAGS="$GOFLAGS -trimpath -mod=vendor"
	go mod vendor
	go build
}

check() {
	go test ./...
}

package() {
	install -Dm755 $pkgname -t "$pkgdir"/usr/bin/
}

sha512sums="
d6eab257ee49d12d07e38b37ae1d60cbf70f627618ea32f2ec74370a09e78cad929de4b7d2bd02e9f300f698d45c8c28f4b4eb337a0be8f29e046fee4bb19b1f  croc-9.6.4.tar.gz
"
