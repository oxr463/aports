# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=usql
pkgver=0.13.12
pkgrel=0
pkgdesc="Universal command-line interface for SQL databases"
url="https://github.com/xo/usql"
# riscv64: not supported by go-ole (undefined: VARIANT)
# x86, armhf, armv7: netezza and cockroachdb drivers fail to build on 32-bit
# - https://github.com/xo/usql/issues/59
# - https://github.com/IBM/nzgo/issues/38
# - https://github.com/cockroachdb/pebble/issues/1575
arch="all !riscv64 !armhf !armv7 !x86"
license="MIT"
makedepends="go unixodbc-dev icu-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/xo/usql/archive/refs/tags/v$pkgver.tar.gz"

export CGO_ENABLED=1 # needed for godror and odbc drivers
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

export _GOTAGS="all sqlite_app_armor sqlite_fts5 sqlite_introspect sqlite_json1 sqlite_stat4 sqlite_userauth sqlite_vtable sqlite_icu"

build() {
	local _goldflags="
	-X github.com/xo/usql/text.CommandName=$pkgname
	-X github.com/xo/usql/text.CommandVersion=$pkgver
	"

	go build -v -ldflags "$_goldflags" -tags "$_GOTAGS" -o $pkgname
}

check() {
	# Tests for specific drivers require docker
	# shellcheck disable=2046
	go test $(go list ./... | grep -v /drivers)
}

package() {
	install -Dm755 $pkgname -t "$pkgdir"/usr/bin/
}

sha512sums="
30f47b954c468c35cd9b01a21e502126b724412aae10fcbf82d055812c046840d89cd688887c12ffcd0a94ea01c421d5db01e19c4ecb085ca6f5dd6df7a3ff3d  usql-0.13.12.tar.gz
"
