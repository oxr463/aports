# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Magnus Sandin <magnus.sandin@gmail.com>
pkgname=py3-pychromecast
pkgver=13.0.5
pkgrel=0
pkgdesc="Python module to talk to Google Chromecast"
url="https://github.com/home-assistant-libs/pychromecast"
arch="noarch"
license="MIT"
depends="
	py3-casttube
	py3-protobuf
	py3-zeroconf
	python3
	"
makedepends="py3-setuptools"
source="https://pypi.python.org/packages/source/P/PyChromecast/PyChromecast-$pkgver.tar.gz"
options="!check" # No tests
builddir="$srcdir/PyChromecast-$pkgver"

provides="py3-chromecast=$pkgver-r$pkgrel" # Backwards compatibility
replaces="py3-chromecast" # Backwards compatibility

build() {
	python3 setup.py build
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"
}

sha512sums="
c02ea4918dc692665a530691f90a28e77d8556151cf891f1472fb6479a6881d2ee8b4c489ec2c5f73a2c59492fff64487f3a0d4d28a4634f4563567916e80346  PyChromecast-13.0.5.tar.gz
"
