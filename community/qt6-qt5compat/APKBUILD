# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qt6-qt5compat
pkgver=6.4.3
pkgrel=0
pkgdesc="Module that contains unsupported Qt 5 APIs"
url="https://qt.io/"
arch="all"
license="LGPL-2.1-only AND LGPL-3.0-only AND GPL-3.0-only AND Qt-GPL-exception-1.0"
# icu-data-full: see https://gitlab.alpinelinux.org/alpine/aports/-/issues/13814
# qt6-qt5compat provides the same icu mib apis
depends="icu-data-full"
depends_dev="
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	"
makedepends="$depends_dev
	cmake
	perl
	samurai
	"
subpackages="$pkgname-dev"
options="!check" # No tests
builddir="$srcdir/qt5compat-everywhere-src-${pkgver/_/-}"

case $pkgver in
	*_alpha*|*_beta*|*_rc*) _rel=development_releases;;
	*) _rel=official_releases;;
esac

source="https://download.qt.io/$_rel/qt/${pkgver%.*}/${pkgver/_/-}/submodules/qt5compat-everywhere-src-${pkgver/_/-}.tar.xz"

build() {
	export CFLAGS="$CFLAGS -g1 -flto=auto"
	export CXXFLAGS="$CXXFLAGS -g1 -flto=auto"
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
22e3d6360afdb83b4bd122153cf3bba09d8593dbdd65ccf5b80e032b9de437f868b05c7170e636fd6bd8f42dd1eb56b07f21b73fd0c4a5437fcb72aa070d830b  qt5compat-everywhere-src-6.4.3.tar.xz
"
