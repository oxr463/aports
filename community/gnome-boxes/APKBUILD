# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=gnome-boxes
pkgver=44.0
pkgrel=0
pkgdesc="Virtualization made simple"
url="https://wiki.gnome.org/Apps/Boxes"
arch="all"
license="GPL-2.0-only"
depends="libvirt-daemon"
makedepends="
	desktop-file-utils
	freerdp-dev
	glib-dev
	gtk+3.0-dev
	gtk-vnc-dev
	gtksourceview4-dev
	itstool
	libarchive-dev
	libgudev-dev
	libhandy1-dev
	libosinfo-dev
	libsecret-dev
	libsoup3-dev
	libusb-dev
	libvirt-glib-dev
	libxml2-dev
	meson
	spice-gtk-dev
	tracker-dev
	vala
	vte3-dev
	webkit2gtk-4.1-dev
	"
depends="
	libvirt-qemu
	qemu
	qemu-chardev-spice
	qemu-img
	qemu-hw-display-qxl
	qemu-hw-display-virtio-gpu
	qemu-hw-display-virtio-vga
	qemu-hw-usb-redirect
	"
case $CARCH in
	x86_64) depends="$depends qemu-system-x86_64";;
	x86) depends="$depends qemu-system-i386";;
	aarch64) depends="$depends qemu-system-aarch64";;
esac
subpackages="$pkgname-lang $pkgname-doc"
source="https://download.gnome.org/sources/gnome-boxes/${pkgver%%.*}/gnome-boxes-$pkgver.tar.xz
	add-smartcard-disable-option.patch
	"

build() {
	abuild-meson \
		-Db_lto=true \
		-Dsmartcard=false \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
9d03547262dfb79bda2f0cc008a127a9bada758e85264ee8d2912b25e5bc116ab7df522ba56b90e598b130714b5dbb0b046b6baa9e4722b6fd298355c5aad7ec  gnome-boxes-44.0.tar.xz
f66ee3b817856ad7a45723c656836f7097419ac0942544aaa04d646bd45480c07615e5a2e2a2987347dd6907fcd25ee9b0ac84bfdc6bbe8cdff919a99484827b  add-smartcard-disable-option.patch
"
